package web.model;

import entity.Test;
import business.TestBean;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@ManagedBean
@RequestScoped
public class TestMBean implements Serializable {

    private String id;
    private String filename;
    private Test testFound;
    private List<Test> listAllTest;

    @EJB
    private TestBean testBean;

    @Inject
    private FacesContext context;

    @PostConstruct
    private void load(){
        getAllTest();
    }

    public void saveTest(){
        if(id == null || filename == null){
            FacesMessage msg = new FacesMessage("Veuillez remplir les deux champs svp.");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(null, msg);
        }
        else {
            testBean.createTest(id, filename);
            FacesMessage msg = new FacesMessage("Vous avez bien créé une entité Test avec " +
                    "id=" + id + " et filename = " + filename);
            msg.setSeverity(FacesMessage.SEVERITY_INFO);
            context.addMessage(null, msg);
        }
        getAllTest();
//        return "index.xhtml";
    }

    public void findTest(){
        testFound = testBean.findTest(id, filename);
        if (testFound == null){
            FacesMessage msg = new FacesMessage("Aucune entité Test trouvée pour la recherche effectuée. ");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage(null, msg);
        }
//        return "index.xhtml";
    }

    public void getAllTest(){
        this.listAllTest = testBean.getAllTest();
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Test getTestFound() {
        return testFound;
    }

    public void setTestFound(Test testFound) {
        this.testFound = testFound;
    }

    public List<Test> getListAllTest() {
        return listAllTest;
    }

    public void setListAllTest(List<Test> listAllTest) {
        this.listAllTest = listAllTest;
    }
}
