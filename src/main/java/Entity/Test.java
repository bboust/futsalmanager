package entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "TEST")
public class Test {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    @Column(name = "FILENAME", nullable = false)
    private String filename;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
