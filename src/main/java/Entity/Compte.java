package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "COMPTE")
public class Compte {

    @Id
    @Column(name = "ID_COMPTE", nullable = false)
    private String idCompte;
    @Column(name = "SOLDE")
    private String solde;
    @Column(name = "BANQUE")
    private String banque;


    public Compte() {
    }

    public String getIdCompte() {
        return idCompte;
    }

    public void setIdCompte(String idCompte) {
        this.idCompte = idCompte;
    }

    public String getSolde() {
        return solde;
    }

    public void setSolde(String solde) {
        this.solde = solde;
    }

    public String getBanque() {
        return banque;
    }

    public void setBanque(String banque) {
        this.banque = banque;
    }
}
