package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "MATCH")
public class Match {

    @Id
    @Column(name = "ID_MATCH", nullable = false)
    private Integer idMatch;
    @Column(name = "DATE")
    private String date;
    @Column(name = "LIEU")
    private String lieu;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "NOM")
    private CentreSportif centreSportif;


    public Match() {
    }

    public Integer getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(Integer idMatch) {
        this.idMatch = idMatch;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLieu() {
        return lieu;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public CentreSportif getCentreSportif() {
        return centreSportif;
    }

    public void setCentreSportif(CentreSportif centreSportif) {
        this.centreSportif = centreSportif;
    }
}
