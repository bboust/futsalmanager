package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "ETAT_INSCRIPTION")
public class EtatInscription {

    @Id
    @Column(name = "ID_ETAT_INSC", nullable = false)
    private Integer idEtatInsc;
    @Column(name = "DESCRIPTION")
    private String description;

    public EtatInscription() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getIdEtatInsc() {
        return idEtatInsc;
    }

    public void setIdEtatInsc(Integer idEtatInsc) {
        this.idEtatInsc = idEtatInsc;
    }
}
