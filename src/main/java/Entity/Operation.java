package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "OPERATION")
public class Operation {

    @Id
    @Column(name = "ID_OPERATION", nullable = false)
    private Integer idOperation;
    @Column(name = "MONTANT")
    private Integer montant;
    @Column(name = "DATE")
    private String date;
    @Column(name = "COMMUNICATION")
    private String communication;
    @OneToOne
    @JoinColumn(name = "ID_COMPTE", referencedColumnName = "ID_COMPTE")
    private Compte compte;


    public Operation() {
    }

    public Integer getIdOperation() {
        return idOperation;
    }

    public void setIdOperation(Integer idOperation) {
        this.idOperation = idOperation;
    }

    public Integer getMontant() {
        return montant;
    }

    public void setMontant(Integer montant) {
        this.montant = montant;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCommunication() {
        return communication;
    }

    public void setCommunication(String communication) {
        this.communication = communication;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }
}
