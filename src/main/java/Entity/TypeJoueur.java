package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "TYPE_JOUEUR")
public class TypeJoueur {

    @Id
    @Column(name = "TYPE", nullable = false)
    private String type;
    @Column(name = "DESCRIPTION")
    private String description;


    public TypeJoueur() {
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
