package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "JOUEUR")
public class Joueur {

    @Id
    @Column(name = "ID_JOUEUR", nullable = false)
    private Integer idJoueur;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "PRENOM")
    private String prenom;
    @Column(name = "NUMERO_MAILLOT")
    private Integer numeroMaillot;
    @Column(name = "POSTE_PREFERE")
    private String postePrefere;
    @OneToOne
    @JoinColumn(name = "ID_COMPTE", referencedColumnName = "ID_COMPTE")
    private Compte compte;
    @OneToOne
    @JoinColumn(name = "LOGIN", referencedColumnName = "LOGIN")
    private Utilisateur utilisateur;


    public Joueur() {
    }

    public Integer getIdJoueur() {
        return idJoueur;
    }

    public void setIdJoueur(Integer idJoueur) {
        this.idJoueur = idJoueur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getNumeroMaillot() {
        return numeroMaillot;
    }

    public void setNumeroMaillot(Integer numeroMaillot) {
        this.numeroMaillot = numeroMaillot;
    }

    public String getPostePrefere() {
        return postePrefere;
    }

    public void setPostePrefere(String postePrefere) {
        this.postePrefere = postePrefere;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }
}
