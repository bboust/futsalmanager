package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "CENTRE_SPORTIF")
public class CentreSportif {

    @Id
    @Column(name = "NOM", nullable = false)
    private String nom;
    @Column(name = "RUE")
    private String rue;
    @Column(name = "NUMERO")
    private Integer numero;
    @Column(name = "CODE_POSTAL")
    private Integer codePostal;
    @Column(name = "COMMUNE")
    private String commune;


    public CentreSportif() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(Integer codePostal) {
        this.codePostal = codePostal;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }
}
