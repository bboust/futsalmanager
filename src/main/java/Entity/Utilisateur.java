package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "UTILISATEUR")
public class Utilisateur {

    @Id
    @Column(name = "LOGIN", nullable = false)
    private String login;
    @Column(name = "MOT_DE_PASSE")
    private String motDePasse;
    @Column(name = "NOM")
    private String nom;
    @Column(name = "PRENOM")
    private String prenom;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "NUMERO_GSM")
    private String numeroGsm;
    @Column(name = "DATE_DEMANDE_INSC")
    private String dateDemandeInsc;
    @OneToOne
    @JoinColumn(name = "ID_JOUEUR", referencedColumnName = "ID_JOUEUR")
    private Joueur joueur;
    @ManyToOne
    @JoinColumn(name = "ID_ETAT_INSC", referencedColumnName = "ID_ETAT_INSC")
    private EtatInscription etatInscription;


    public Utilisateur() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumeroGsm() {
        return numeroGsm;
    }

    public void setNumeroGsm(String numeroGsm) {
        this.numeroGsm = numeroGsm;
    }

    public String getDateDemandeInsc() {
        return dateDemandeInsc;
    }

    public void setDateDemandeInsc(String dateDemandeInsc) {
        this.dateDemandeInsc = dateDemandeInsc;
    }

    public Joueur getJoueur() {
        return joueur;
    }

    public void setJoueur(Joueur joueur) {
        this.joueur = joueur;
    }

    public EtatInscription getEtatInscription() {
        return etatInscription;
    }

    public void setEtatInscription(EtatInscription etatInscription) {
        this.etatInscription = etatInscription;
    }
}
