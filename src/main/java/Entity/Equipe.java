package entity;

import javax.persistence.*;

/**
 * Created by bmottiaux on 29/07/2015.
 */
@Entity
@Table(name = "EQUIPE")
public class Equipe {

    @Id
    @Column(name = "NOM_EQUIPE", nullable = false)
    private String nomEquipe;
    @Column(name = "LIGUE")
    private String ligue;
    @Column(name = "DIVISION")
    private String division;
    @OneToOne
    @JoinColumn(name = "ID_COMPTE", referencedColumnName = "ID_COMPTE")
    private Compte compte;
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "NOM")
    private CentreSportif centreSportif;

    public Equipe() {
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public void setNomEquipe(String nomEquipe) {
        this.nomEquipe = nomEquipe;
    }

    public String getLigue() {
        return ligue;
    }

    public void setLigue(String ligue) {
        this.ligue = ligue;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    public CentreSportif getCentreSportif() {
        return centreSportif;
    }

    public void setCentreSportif(CentreSportif centreSportif) {
        this.centreSportif = centreSportif;
    }
}
