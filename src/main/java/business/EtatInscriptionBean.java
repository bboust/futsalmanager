package business;

import entity.EtatInscription;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class EtatInscriptionBean {

    @PersistenceContext
    private EntityManager em;


    public EtatInscription create(EtatInscription e){
        return em.merge(e);
    }

    public EtatInscription find(Integer idEtatInsc){
        return em.createQuery("select e from EtatInscription e where e.idEtatInsc = :idEtatInsc", EtatInscription.class)
                .setParameter("idEtatInsc", idEtatInsc)
                .getSingleResult();
    }

    public List<EtatInscription> findAll(){
        return em.createQuery("select e from EtatInscription e", EtatInscription.class)
                .getResultList();
    }

}
