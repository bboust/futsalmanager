package business;

import entity.Joueur;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class JoueurBean {

    @PersistenceContext
    private EntityManager em;


    public Joueur create(Joueur j){
        return em.merge(j);
    }

    public Joueur find(Integer idJoueur){
        return em.createQuery("select j from Joueur j where j.idJoueur = :idJoueur", Joueur.class)
                .setParameter("idJoueur", idJoueur)
                .getSingleResult();
    }

    public List<Joueur> findAll(){
        return em.createQuery("select j from Joueur j", Joueur.class)
                .getResultList();
    }

}
