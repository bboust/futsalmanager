package business;

import entity.CentreSportif;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class CentreSportifBean {

    @PersistenceContext
    private EntityManager em;


    public CentreSportif create(CentreSportif cs){
        return em.merge(cs);
    }

    public CentreSportif find(String nom){
        return em.createQuery("select cs from CentreSportif cs where cs.nom = :nom", CentreSportif.class)
                .setParameter("nom", nom)
                .getSingleResult();
    }

    public List<CentreSportif> findAll(){
        return em.createQuery("select cs from CentreSportif cs", CentreSportif.class)
                .getResultList();
    }

}
