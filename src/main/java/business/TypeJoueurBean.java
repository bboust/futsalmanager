package business;


import entity.TypeJoueur;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class TypeJoueurBean {

    @PersistenceContext
    private EntityManager em;


    public TypeJoueur create(TypeJoueur tj){
        return em.merge(tj);
    }

    public TypeJoueur find(String type){
        return em.createQuery("select tj from TypeJoueur tj where tj.type = :typeJoueur", TypeJoueur.class)
                .setParameter("typeJoueur", type)
                .getSingleResult();
    }

    public List<TypeJoueur> findAll(){
        return em.createQuery("select tj from TypeJoueur tj", TypeJoueur.class)
                .getResultList();
    }

}
