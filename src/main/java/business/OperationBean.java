package business;


import entity.Operation;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class OperationBean {

    @PersistenceContext
    private EntityManager em;


    public Operation create(Operation o){
        return em.merge(o);
    }

    public Operation find(Integer idOperation){
        return em.createQuery("select o from Operation o where o.idOperation = :idOperation", Operation.class)
                .setParameter("idOperation", idOperation)
                .getSingleResult();
    }

    public List<Operation> findAll(){
        return em.createQuery("select o from Operation o", Operation.class)
                .getResultList();
    }

}
