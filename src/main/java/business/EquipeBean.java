package business;

import entity.Equipe;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class EquipeBean {

    @PersistenceContext
    private EntityManager em;


    public Equipe create(Equipe e){
        return em.merge(e);
    }

    public Equipe find(String nomEquipe){
        return em.createQuery("select e from Equipe e where e.nomEquipe = :nomEquipe", Equipe.class)
                .setParameter("nomEquipe", nomEquipe)
                .getSingleResult();
    }

    public List<Equipe> findAll(){
        return em.createQuery("select e from Equipe e", Equipe.class)
                .getResultList();
    }

}
