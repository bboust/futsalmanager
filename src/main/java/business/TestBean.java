package business;

import entity.Test;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class TestBean{

    @PersistenceContext
    private EntityManager em;

//    public TestBean(Class entityClass) {
//        super(entityClass);
//    }

    public Test createTest(String id, String filename){
        Test test = (Test) this.create();
        test.setId(id);
        test.setFilename(filename);
        return em.merge(test);
    }

    public Test create() {
        Test newInstance = null;

        try {
            newInstance = Test.class.newInstance();
        } catch (IllegalAccessException | InstantiationException var3) {
//            LOGGER.log(Level.SEVERE, (String)null, var3);
            System.out.println("Erreur durant l'instanciation de Test");
        }

        return newInstance;
    }

    public Test findTest(String id, String filename){
        return em.createQuery("select t from Test t where t.id = :id and t.filename =:filename", Test.class)
                .setParameter("id", id)
                .setParameter("filename", filename)
                .getSingleResult();
    }

    public List<Test> getAllTest(){
        return em.createQuery("select t from Test t")
                .getResultList();
    }

//    @Override
//    protected EntityManager getEntityManager() {
//        return em;
//    }
}
