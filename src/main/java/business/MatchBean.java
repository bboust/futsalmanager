package business;


import entity.Match;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class MatchBean {

    @PersistenceContext
    private EntityManager em;


    public Match create(Match m){
        return em.merge(m);
    }

    public Match find(Integer idMatch){
        return em.createQuery("select m from Match m where m.idMatch = :idMatch", Match.class)
                .setParameter("idMatch", idMatch)
                .getSingleResult();
    }

    public List<Match> findAll(){
        return em.createQuery("select j from Match j", Match.class)
                .getResultList();
    }

}
