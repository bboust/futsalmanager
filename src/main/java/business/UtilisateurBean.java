package business;


import entity.Utilisateur;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class UtilisateurBean {

    @PersistenceContext
    private EntityManager em;


    public Utilisateur create(Utilisateur u){
        return em.merge(u);
    }

    public Utilisateur find(String login){
        return em.createQuery("select u from Utilisateur u where u.login = :login", Utilisateur.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    public List<Utilisateur> findAll(){
        return em.createQuery("select u from Utilisateur u", Utilisateur.class)
                .getResultList();
    }

}
