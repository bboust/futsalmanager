package business;

import entity.Compte;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by bmottiaux on 31/07/2015.
 */
@Stateless
public class CompteBean {

    @PersistenceContext
    private EntityManager em;


    public Compte create(Compte c){
        return em.merge(c);
    }

    public Compte find(Integer idCompte){
        return em.createQuery("select c from Compte c where c.idCompte = :idCompte", Compte.class)
                .setParameter("idCompte", idCompte)
                .getSingleResult();
    }

    public List<Compte> findAll(){
        return em.createQuery("select c from Compte c", Compte.class)
                .getResultList();
    }

}
